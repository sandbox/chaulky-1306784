/**
 * Attach behaviors to the gallery.
 */
// This behavior initializes the Galleriffic plug-in.
Drupal.behaviors.gallerifficFormatter = {
  attach: function(context, settings){			
  	// Initialize Minimal Galleriffic Gallery
  	jQuery('#galleriffic-thumbs').galleriffic({
  		imageContainerSel:     '#image-container',
  		enableBottomPager:     false,
  		enableTopPager:        false,
  		controlsContainerSel:  '#controls',
  		captionContainerSel:   '#caption',
  		nextLinkText:          'Next &rsaquo;&rsaquo;',
      prevLinkText:          '&lsaquo;&lsaquo; Prev',
  		renderSSControls:       false,
  		numThumbs:              6
  	});
  }
};

